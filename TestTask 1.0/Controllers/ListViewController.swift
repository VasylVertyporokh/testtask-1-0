//
//  ListViewController.swift
//  TestTask 1.0
//
//  Created by Vasil Vertiporokh on 07.06.2022.
//

import UIKit

final class ListViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var listImage: UIImageView!
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - Private properties
    private var usersInfoArray: [UsersInfoResponseModel]?
    
    // MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "MainUsersInfoCell", bundle: nil), forCellReuseIdentifier: "MainUsersInfoCell")
        configureUI()
        getUsersList()
    }
    
    // MARK: - Private methods
    private func configureUI() {
        title = "Users List"
        listImage.image = UIImage(named: "listIcon")
    }
    
    private func getUsersList() {
        ApiManager.shared.getUserInformation { [weak self] result in
            self?.usersInfoArray = result
            self?.tableView.reloadData()
        }
    }
}

// MARK: - Extensions
extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersInfoArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainUsersInfoCell",
                                                       for: indexPath) as? MainUsersInfoCell else {
            return UITableViewCell()
        }
        
        cell.setDetail(usersInfoArray?[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let usersInfo = usersInfoArray?[indexPath.row],
              let userDetailsViewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "UserDetailsViewController") as? UserDetailsViewController else {
            return
        }
        
        userDetailsViewController.selectedUserInfo = usersInfo
        navigationController?.pushViewController(userDetailsViewController, animated: true)
    }
}

