//
//  UserDetailsViewController.swift
//  TestTask 1.0
//
//  Created by Vasil Vertiporokh on 07.06.2022.
//

import Foundation
import UIKit

final class UserDetailsViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var userDetailInfoView: UserDetailsView!
    
    // MARK: - Public properties
    var selectedUserInfo: UsersInfoResponseModel?
    
    // MARK: - Private properties
    private let randomImageArray = [
        UIImage(named: "user1"),
        UIImage(named: "user2"),
        UIImage(named: "user3"),
        UIImage(named: "user4")
    ]
    
    // MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    // MARK: - Private methods
    private func configureUI() {
        if let randomElement = randomImageArray.randomElement() {
            userDetailInfoView.setUserDetailInfo(selectedUserInfo, randomElement)
        }
        title = "User Details"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .automatic
    }
}
