//
//  ApiManager.swift
//  TestTask 1.0
//
//  Created by Vasil Vertiporokh on 07.06.2022.
//

import Foundation

final class ApiManager {
    static let shared = ApiManager()
    
    private init () {}
    
    func getUserInformation(completion: (([UsersInfoResponseModel]) -> Void)? = nil) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/users/")
        
        guard let url = url else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            guard let data = data else {
                print("❌-----Data is nil something went wrong ----❌")
                return
            }
            if let error = error {
                print("❌-----Api Error - \(error.localizedDescription) ----❌")
            }
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let userInfo = try JSONDecoder().decode([UsersInfoResponseModel].self, from: data)
                let rawJson = try JSONSerialization.jsonObject(with: data, options: [])
                print("✅_____________________________________________✅")
                print(rawJson)
                print("✅_____________________________________________✅")
                DispatchQueue.main.async {
                    completion?(userInfo)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }.resume()
    }
}
