//
//  UsersInfoResponseModel.swift
//  TestTask 1.0
//
//  Created by Vasil Vertiporokh on 07.06.2022.
//

import Foundation

struct UsersInfoResponseModel: Decodable {
    let userId: Int
    let name: String
    let userName: String
    let email: String
    let address: Address
    let phone: String
    let website: String
    let company: Company
    
    enum CodingKeys: String, CodingKey {
        case userId = "id"
        case name
        case userName = "username"
        case email
        case address
        case phone
        case website
        case company
    }
}
