//
//  Company.swift
//  TestTask 1.0
//
//  Created by Vasil Vertiporokh on 07.06.2022.
//

import Foundation

struct Company: Decodable {
    let name: String
    let catchPhrase: String
    let business: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case catchPhrase
        case business = "bs"
    }
}
