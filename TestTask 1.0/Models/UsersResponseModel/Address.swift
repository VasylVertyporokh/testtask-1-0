//
//  Address.swift
//  TestTask 1.0
//
//  Created by Vasil Vertiporokh on 07.06.2022.
//

import Foundation

struct Address: Decodable {
    let street: String
    let suite: String
    let city: String
    let zipCode: String
    let geo: Geo
    
    enum CodingKeys: String, CodingKey {
        case street
        case suite
        case city
        case zipCode = "zipcode"
        case geo 
    }
}
