//
//  Geo.swift
//  TestTask 1.0
//
//  Created by Vasil Vertiporokh on 07.06.2022.
//

import Foundation

struct Geo: Decodable {
    let latitude: String
    let longitude: String
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
}
