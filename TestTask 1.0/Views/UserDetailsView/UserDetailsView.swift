//
//  UserDetailsView.swift
//  TestTask 1.0
//
//  Created by Vasil Vertiporokh on 08.06.2022.
//

import Foundation
import UIKit

final class UserDetailsView: UIView {
    // MARK: - Outlets
    @IBOutlet private var contentView: UIView!
    
    @IBOutlet private weak var userPhotoView: UIImageView!
    @IBOutlet private weak var userNameLabel: UILabel!
    
    @IBOutlet private weak var contactPlaceholded: UILabel!
    @IBOutlet private weak var phoneNumberLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var webSiteLabel: UILabel!
    
    @IBOutlet private weak var addressPlaceholder: UILabel!
    @IBOutlet private weak var streetLabel: UILabel!
    @IBOutlet private weak var suiteLabel: UILabel!
    @IBOutlet private weak var cityLabel: UILabel!
    @IBOutlet private weak var zipCodeLabel: UILabel!
    @IBOutlet private weak var latitudeLabel: UILabel!
    @IBOutlet private weak var longitudeLabel: UILabel!
    
    @IBOutlet private weak var companyPlaceholder: UILabel!
    @IBOutlet private weak var companyNamePlaceholder: UILabel!
    @IBOutlet private weak var catchPhrasePlaceholder: UILabel!
    @IBOutlet private weak var typeOfBussinesPlaceholder: UILabel!
    @IBOutlet private weak var companyNameLabel: UILabel!
    @IBOutlet private weak var catchPhraseLabel: UILabel!
    @IBOutlet private weak var typeOfBussines: UILabel!
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureUI()
    }
    
    // MARK: - Private methods
    private func configureUI() {
        Bundle.main.loadNibNamed(String(describing: UserDetailsView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            contentView.leftAnchor.constraint(equalTo: leftAnchor)
        ])
        
        userPhotoView.contentMode = .scaleAspectFill
        userPhotoView.layer.cornerRadius = userPhotoView.bounds.height / 2
        contactPlaceholded.text = "Contacts"
        addressPlaceholder.text = "Address"
        companyPlaceholder.text = "Company"
        companyNamePlaceholder.text = "Company name:"
        catchPhrasePlaceholder.text = "Catch Phrase:"
        typeOfBussinesPlaceholder.text = "Type of business:"
    }
    
    // MARK: - Public methods
    func setUserDetailInfo(_ userDetailInfoModel: UsersInfoResponseModel?, _ userPhoto: UIImage?) {
        guard let userDetailInfoModel = userDetailInfoModel else {
            return
        }
        userPhotoView.image = userPhoto
        userNameLabel.text = userDetailInfoModel.name
        phoneNumberLabel.text = userDetailInfoModel.phone
        emailLabel.text = userDetailInfoModel.email
        webSiteLabel.text = userDetailInfoModel.website
        streetLabel.text = userDetailInfoModel.address.street
        suiteLabel.text = userDetailInfoModel.address.suite
        cityLabel.text = userDetailInfoModel.address.city
        zipCodeLabel.text = userDetailInfoModel.address.zipCode
        latitudeLabel.text = userDetailInfoModel.address.geo.latitude
        longitudeLabel.text = userDetailInfoModel.address.geo.longitude
        companyNameLabel.text = userDetailInfoModel.company.name
        catchPhraseLabel.text = userDetailInfoModel.company.catchPhrase
        typeOfBussines.text = userDetailInfoModel.company.business
    }
}
