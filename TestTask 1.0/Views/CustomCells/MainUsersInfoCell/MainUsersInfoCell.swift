//
//  MainUsersInfoCell.swift
//  TestTask 1.0
//
//  Created by Vasil Vertiporokh on 07.06.2022.
//

import Foundation
import UIKit

final class MainUsersInfoCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet private weak var userIdPlaceholder: UILabel!
    @IBOutlet private weak var userId: UILabel!
    @IBOutlet private weak var userName: UILabel!
    @IBOutlet private weak var goToDetailImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }
    
    // MARK: - Private methods
    private func configureCell() {
        userIdPlaceholder.text = "ID:"
        goToDetailImage.image = UIImage(named: "arrow")
        selectionStyle = .none
    }
    
    // MARK: - Public methods
    func setDetail(_ userModel: UsersInfoResponseModel?) {
        guard let userModel = userModel else {
            return
        }
        
        userId.text = "\(userModel.userId)"
        userName.text = userModel.userName
    }
}
